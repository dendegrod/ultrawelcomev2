package me.sharkz.ultrawelcome.spigot;

import me.sharkz.ultrawelcome.common.player.AbstractUwPlayer;
import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import me.sharkz.ultrawelcome.spigot.event.SpigotJoinEvent;
import me.sharkz.ultrawelcome.spigot.event.SpigotQuitEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;
import java.util.logging.Logger;

public class UwSpigot extends JavaPlugin implements UwPlugin {

    // Map of registered players
    private final HashMap<UUID, AbstractUwPlayer> players = new HashMap<>();

    @Override
    public void onEnable() {
        // Register events
        registerEvents(
                new SpigotJoinEvent(this),
                new SpigotQuitEvent(this)
        );
    }

    @Override
    public void onDisable() {
    }

    @Override
    public UwPlugin getPlugin() {
        return this;
    }

    @Override
    public Logger getLogging() {
        return getLogger();
    }

    @Override
    public Collection<AbstractUwPlayer> getPlayers() {
        return this.players.values();
    }

    @Override
    public void addPlayer(AbstractUwPlayer player) {
        this.players.put(player.getUniqueId(), player);
    }

    @Override
    public AbstractUwPlayer getPlayer(UUID uuid) {
        return this.players.get(uuid);
    }

    @Override
    public boolean isRegistered(UUID uuid) {
        return this.players.containsKey(uuid);
    }

    /**
     * Register a list of events/listeners
     *
     * @param events List of events/listeners
     */
    private void registerEvents(Listener... events) {
        for (Listener event : events) {
            Bukkit.getPluginManager().registerEvents(event, this);
        }
    }
}
