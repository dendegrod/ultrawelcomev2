package me.sharkz.ultrawelcome.spigot.event;

import me.sharkz.ultrawelcome.spigot.UwSpigot;
import me.sharkz.ultrawelcome.spigot.player.SpigotPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class SpigotQuitEvent implements Listener {

    private final UwSpigot plugin;

    public SpigotQuitEvent(UwSpigot plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void on(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        SpigotPlayer spigotPlayer = (SpigotPlayer) plugin.getPlayer(player.getUniqueId());

        // Execute other stuff

        spigotPlayer.setBukkitPlayer(null);
    }

}
