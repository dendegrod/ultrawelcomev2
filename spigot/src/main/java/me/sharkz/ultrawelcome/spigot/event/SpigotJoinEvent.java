package me.sharkz.ultrawelcome.spigot.event;

import me.sharkz.ultrawelcome.spigot.UwSpigot;
import me.sharkz.ultrawelcome.spigot.player.SpigotPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class SpigotJoinEvent implements Listener {

    private final UwSpigot plugin;

    public SpigotJoinEvent(UwSpigot plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void on(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        SpigotPlayer spigotPlayer;
        // Get player
        if (!plugin.isRegistered(player.getUniqueId())) {
            // Register player if not present
            spigotPlayer = new SpigotPlayer(plugin, e.getPlayer().getUniqueId());
            spigotPlayer.setBukkitPlayer(player);
            plugin.addPlayer(spigotPlayer);
        } else {
            spigotPlayer = (SpigotPlayer) plugin.getPlayer(player.getUniqueId());
        }

        // Execute other stuff
    }

}
