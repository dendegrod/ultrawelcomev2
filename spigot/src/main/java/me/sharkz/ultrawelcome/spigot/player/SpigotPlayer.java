package me.sharkz.ultrawelcome.spigot.player;

import me.sharkz.ultrawelcome.common.player.AbstractUwPlayer;
import me.sharkz.ultrawelcome.spigot.UwSpigot;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

public class SpigotPlayer extends AbstractUwPlayer {

    private final UwSpigot plugin;
    private final UUID player;
    private Player bukkitPlayer;

    public SpigotPlayer(UwSpigot plugin, UUID player) {
        this.plugin = plugin;
        this.player = player;

        this.bukkitPlayer = Bukkit.getPlayer(player);
    }

    @Override
    public boolean isOffline() {
        return bukkitPlayer == null;
    }

    @Override
    public UUID getUniqueId() {
        return player;
    }

    @Override
    public String getName() {
        return bukkitPlayer.getName();
    }

    @Override
    public boolean hasPermission(@Nullable String permission) {
        return bukkitPlayer.hasPermission(permission);
    }

    @Override
    public void sendMessage(String message) {
        //TODO Implement sendMessage
    }

    /**
     * Get the bukkit instance of the player
     *
     * @return Bukkit instance of the player
     */
    public Player getBukkitPlayer() {
        return bukkitPlayer;
    }

    /**
     * Set the bukkit instance of the player
     *
     * @param bukkitPlayer Bukkit instance of the player
     */
    public void setBukkitPlayer(Player bukkitPlayer) {
        this.bukkitPlayer = bukkitPlayer;
    }
}
