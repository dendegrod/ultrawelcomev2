package me.sharkz.ultrawelcome.bungee.player;

import me.sharkz.ultrawelcome.bungee.UwBungee;
import me.sharkz.ultrawelcome.common.player.AbstractUwPlayer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

public class BungeePlayer extends AbstractUwPlayer {

    private final UwBungee plugin;
    private final UUID player;
    private ProxiedPlayer proxiedPlayer;

    public BungeePlayer(UwBungee plugin, UUID player) {
        this.plugin = plugin;
        this.player = player;

        this.proxiedPlayer = plugin.getProxy().getPlayer(player);
    }

    @Override
    public boolean isOffline() {
        return proxiedPlayer == null;
    }

    @Override
    public UUID getUniqueId() {
        return player;
    }

    @Override
    public boolean hasPermission(@Nullable String permission) {
        return proxiedPlayer.hasPermission(permission);
    }

    @Override
    public String getName() {
        return proxiedPlayer.getName();
    }

    @Override
    public void sendMessage(String message) {
        //TODO Implement sendMessage
    }

    /**
     * Get the bungee instance of the player
     *
     * @return Bungee instance of the player
     */
    public ProxiedPlayer getProxiedPlayer() {
        return proxiedPlayer;
    }

    /**
     * Set the bungee instance of the player
     *
     * @param proxiedPlayer Bungee instance of the player
     */
    public void setProxiedPlayer(@Nullable ProxiedPlayer proxiedPlayer) {
        this.proxiedPlayer = proxiedPlayer;
    }
}
