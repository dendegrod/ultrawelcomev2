package me.sharkz.ultrawelcome.bungee.event;

import me.sharkz.ultrawelcome.bungee.UwBungee;
import me.sharkz.ultrawelcome.bungee.player.BungeePlayer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class BungeeJoinEvent implements Listener {

    private final UwBungee plugin;

    public BungeeJoinEvent(UwBungee plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void on(PostLoginEvent e) {
        ProxiedPlayer player = e.getPlayer();
        BungeePlayer bungeePlayer;
        // Get player
        if (!plugin.isRegistered(player.getUniqueId())) {
            // Register player if not present
            bungeePlayer = new BungeePlayer(plugin, player.getUniqueId());
            bungeePlayer.setProxiedPlayer(player);
            plugin.addPlayer(bungeePlayer);
        } else {
            bungeePlayer = (BungeePlayer) plugin.getPlayer(player.getUniqueId());
        }

        // Execute other things
    }

}
