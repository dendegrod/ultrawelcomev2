package me.sharkz.ultrawelcome.bungee.event;

import me.sharkz.ultrawelcome.bungee.UwBungee;
import me.sharkz.ultrawelcome.bungee.player.BungeePlayer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class BungeeQuitEvent implements Listener {

    private final UwBungee plugin;

    public BungeeQuitEvent(UwBungee plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void on(PlayerDisconnectEvent e) {
        ProxiedPlayer player = e.getPlayer();
        BungeePlayer bungeePlayer = (BungeePlayer) plugin.getPlayer(player.getUniqueId());

        // Execute other things

        bungeePlayer.setProxiedPlayer(null);
    }

}
