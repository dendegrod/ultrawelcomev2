package me.sharkz.ultrawelcome.bungee;

import me.sharkz.ultrawelcome.bungee.event.BungeeJoinEvent;
import me.sharkz.ultrawelcome.bungee.event.BungeeQuitEvent;
import me.sharkz.ultrawelcome.common.player.AbstractUwPlayer;
import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Logger;

public class UwBungee extends Plugin implements UwPlugin {

    // Map of registered players
    private final HashMap<UUID, AbstractUwPlayer> players = new HashMap<>();

    @Override
    public void onEnable() {
        // Register events
        registerEvents(
                new BungeeJoinEvent(this),
                new BungeeQuitEvent(this)
        );
    }

    @Override
    public void onDisable() {
    }

    @Override
    public UwPlugin getPlugin() {
        return this;
    }

    @Override
    public Logger getLogging() {
        return getLogger();
    }

    @Override
    public Collection<AbstractUwPlayer> getPlayers() {
        return this.players.values();
    }

    @Override
    public void addPlayer(AbstractUwPlayer player) {
        this.players.put(player.getUniqueId(), player);
    }

    @Override
    public AbstractUwPlayer getPlayer(UUID uuid) {
        return this.players.get(uuid);
    }

    @Override
    public boolean isRegistered(UUID uuid) {
        return this.players.containsKey(uuid);
    }

    /**
     * Register a list of events/listeners
     *
     * @param events List of events/listeners
     */
    private void registerEvents(Listener... events) {
        for (Listener event : events) {
            getProxy().getPluginManager().registerListener(this, event);
        }
    }
}
