-- UltraWelcome PostgreSQL Schema

CREATE TABLE IF NOT EXISTS "{prefix}players" (
  "uuid"        VARCHAR(36) NOT NULL,
  "username"    VARCHAR(16) NOT NULL,
  "identifier"  INT         NOT NULL,
  "firstJoin"   TIMESTAMP   NOT NULL,
  "lastQuit"    TIMESTAMP   NOT NULL,
  "status"      BOOLEAN     NOT NULL,
  PRIMARY KEY ("uuid")
);
CREATE INDEX "{prefix}players_uuid" ON "{prefix}players_uuid" ("uuid");