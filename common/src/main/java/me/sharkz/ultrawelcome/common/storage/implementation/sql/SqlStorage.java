package me.sharkz.ultrawelcome.common.storage.implementation.sql;

import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import me.sharkz.ultrawelcome.common.storage.implementation.StorageImplementation;
import me.sharkz.ultrawelcome.common.storage.implementation.sql.connection.ConnectionFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SqlStorage implements StorageImplementation {

    private final UwPlugin plugin;
    private final ConnectionFactory connectionFactory;
    private final Function<String, String> statementProcessor;

    public SqlStorage(UwPlugin plugin, ConnectionFactory connectionFactory, String tablePrefix) {
        this.plugin = plugin;
        this.connectionFactory = connectionFactory;
        this.statementProcessor = connectionFactory.getStatementProcessor().compose(s -> s.replace("{prefix}", tablePrefix));
    }

    @Override
    public UwPlugin getPlugin() {
        return this.plugin;
    }

    @Override
    public String getImplementationName() {
        return this.connectionFactory.getImplementationName();
    }

    @Override
    public void init() {
        this.connectionFactory.init(this.plugin);

        applySchema();
    }

    @Override
    public void shutdown() {
        try {
            this.connectionFactory.shutdown();
        } catch (Exception e) {
            this.plugin.getLogging().severe("Exception whilst disabling SQL storage");
            e.printStackTrace();
        }
    }

    private void applySchema() {
        List<String> statements = new ArrayList<>();

        String schemaFileName = "/schema/" + this.connectionFactory.getImplementationName().toLowerCase(Locale.ROOT) + ".sql";
        try (InputStream is = this.plugin.getResourceAsStream(schemaFileName)) {
            if (is == null) {
                throw new IOException("Couldn't locate schema file for " + this.connectionFactory.getImplementationName());
            }

            statements = SchemaReader.getStatements(is).stream()
                    .map(getStatementProcessor())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (Connection connection = this.connectionFactory.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                for (String query : statements) {
                    statement.addBatch(query);
                }

                statement.executeBatch();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Function<String, String> getStatementProcessor() {
        return this.statementProcessor;
    }


}
