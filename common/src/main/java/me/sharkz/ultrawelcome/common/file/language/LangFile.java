package me.sharkz.ultrawelcome.common.file.language;

import me.sharkz.ultrawelcome.common.file.SimpleYamlFile;
import me.sharkz.ultrawelcome.common.plugin.UwPlugin;

public class LangFile extends SimpleYamlFile {

    private final String locale;

    public LangFile(UwPlugin plugin, String locale) {
        super(plugin, "lang/" + locale + ".yml", true);

        this.locale = locale;

        // Set default messages if not present
        for (Messages message : Messages.values()) {
            if (!yamlFile.isSet(message.getPath())) yamlFile.set(message.getPath(), message.getValue());
        }
    }

    public String getLocale() {
        return this.locale;
    }

    public String getMessage(String path) {
        return yamlFile.getString(path);
    }

}
