package me.sharkz.ultrawelcome.common.storage.implementation.file;

import me.sharkz.ultrawelcome.common.file.SimpleYamlFile;
import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import me.sharkz.ultrawelcome.common.storage.implementation.StorageImplementation;

public class YamlStorage extends SimpleYamlFile implements StorageImplementation {

    private final UwPlugin plugin;

    public YamlStorage(UwPlugin plugin) {
        super(plugin, "data/data.yml", false);

        this.plugin = plugin;
    }

    @Override
    public UwPlugin getPlugin() {
        return this.plugin;
    }

    @Override
    public String getImplementationName() {
        return "YAML";
    }

    @Override
    public void init() {

    }

    @Override
    public void shutdown() {
        save();
    }
}
