package me.sharkz.ultrawelcome.common.storage.implementation.mongodb;

import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Indexes;
import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import me.sharkz.ultrawelcome.common.storage.implementation.StorageImplementation;
import me.sharkz.ultrawelcome.common.storage.misc.StorageCredentials;
import org.bson.Document;
import org.bson.UuidRepresentation;

public class MongoStorage implements StorageImplementation {

    private final UwPlugin plugin;

    private final StorageCredentials credentials;
    private MongoClient mongoClient;
    private MongoDatabase database;
    private final String prefix;
    private final String connectionUri;

    public MongoStorage(UwPlugin plugin, StorageCredentials credentials, String prefix, String connectionUri) {
        this.plugin = plugin;
        this.credentials = credentials;
        this.prefix = prefix;
        this.connectionUri = connectionUri;
    }

    @Override
    public UwPlugin getPlugin() {
        return this.plugin;
    }

    @Override
    public String getImplementationName() {
        return "MongoDB";
    }

    @Override
    public void init() {
        MongoClientOptions.Builder options = MongoClientOptions.builder()
                .uuidRepresentation(UuidRepresentation.JAVA_LEGACY);

        if (this.connectionUri != null && !this.connectionUri.isEmpty()) {
            this.mongoClient = new MongoClient(new MongoClientURI(this.connectionUri, options));
        } else {
            MongoCredential credential = null;
            if (this.credentials.getUsername() != null && !this.credentials.getUsername().isEmpty()) {
                credential = MongoCredential.createCredential(
                        this.credentials.getUsername(),
                        this.credentials.getDatabase(),
                        this.credentials.getPassword() == null || this.credentials.getPassword().isEmpty() ? null : this.credentials.getPassword().toCharArray()
                );
            }

            String[] addressSplit = this.credentials.getAddress().split(":");
            String host = addressSplit[0];
            int port = addressSplit.length > 1 ? Integer.parseInt(addressSplit[1]) : 27017;
            ServerAddress address = new ServerAddress(host, port);

            if (credential == null) {
                this.mongoClient = new MongoClient(address, options.build());
            } else {
                this.mongoClient = new MongoClient(address, credential, options.build());
            }
        }

        this.database = this.mongoClient.getDatabase(this.credentials.getDatabase());
    }

    @Override
    public void shutdown() {
        if (this.mongoClient != null) {
            this.mongoClient.close();
        }
    }
}
