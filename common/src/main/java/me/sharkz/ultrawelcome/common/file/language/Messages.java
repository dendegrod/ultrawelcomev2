package me.sharkz.ultrawelcome.common.file.language;

public enum Messages {

    ;

    private final String path;
    private final String value;
    Messages(String path, String value) {
        this.path = path;
        this.value = value;
    }

    public String getPath() {
        return path;
    }

    public String getValue() {
        return value;
    }
}
