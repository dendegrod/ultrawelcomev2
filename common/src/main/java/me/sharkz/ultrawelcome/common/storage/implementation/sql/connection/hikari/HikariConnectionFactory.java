package me.sharkz.ultrawelcome.common.storage.implementation.sql.connection.hikari;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import me.sharkz.ultrawelcome.common.storage.implementation.sql.connection.ConnectionFactory;
import me.sharkz.ultrawelcome.common.storage.misc.PoolSettings;
import me.sharkz.ultrawelcome.common.storage.misc.StorageCredentials;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public abstract class HikariConnectionFactory implements ConnectionFactory {

    private final StorageCredentials credentials;
    private HikariDataSource hikari;

    public HikariConnectionFactory(StorageCredentials credentials) {
        this.credentials = credentials;
    }

    protected abstract String defaultPort();

    protected abstract void configureDatabase(HikariConfig config, String address, String port, String database, String username, String password);

    protected void overrideProperties(Map<String, String> properties) {
        properties.putIfAbsent("socketTimeout", String.valueOf(TimeUnit.SECONDS.toMillis(30)));
    }

    protected void setProperties(HikariConfig config, Map<String, String> properties) {
        for (Map.Entry<String, String> property : properties.entrySet()) {
            config.addDataSourceProperty(property.getKey(), property.getValue());
        }
    }

    protected void postInitialize() {

    }

    @Override
    public void init(UwPlugin plugin) {
        HikariConfig config = new HikariConfig();
        
        config.setPoolName("ultrawelcome-hikari");

        String[] addressSplit = this.credentials.getAddress().split(":");
        String address = addressSplit[0];
        String port = addressSplit.length > 1 ? addressSplit[1] : defaultPort();

        configureDatabase(config, address, port, this.credentials.getDatabase(), this.credentials.getUsername(), this.credentials.getPassword());

        Map<String, String> properties = new HashMap<>(plugin.getConfig().getStorageProperties());
        overrideProperties(properties);
        setProperties(config, properties);

        PoolSettings poolSettings = plugin.getConfig().getStoragePoolSettings();
        config.setMaximumPoolSize(poolSettings.getMaximumPoolSize());
        config.setMinimumIdle(poolSettings.getMinimumIdle());
        config.setMaxLifetime(poolSettings.getMaxLifetime());
        config.setKeepaliveTime(poolSettings.getKeepaliveTime());
        config.setConnectionTimeout(poolSettings.getConnectionTimeout());

        config.setInitializationFailTimeout(-1);

        this.hikari = new HikariDataSource(config);

        postInitialize();
    }

    @Override
    public void shutdown() {
        if (this.hikari != null) {
            this.hikari.close();
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (this.hikari == null) {
            throw  new SQLException("Unable to get a connection from the poll. (hikari is null)");
        }

        Connection connection = this.hikari.getConnection();
        if (connection == null) {
            throw new SQLException("Unable to get a connection from the pool. (getConnection return null)");
        }

        return connection;
    }
}
