package me.sharkz.ultrawelcome.common.file.language;

import me.sharkz.ultrawelcome.common.plugin.UwPlugin;

import java.util.ArrayList;
import java.util.List;

// TODO GUI or Command to configure the lang
public class LangManager {

    private final UwPlugin plugin;

    private final Mode mode;
    private final String language;
    private final List<String> languages;
    private final List<LangFile> langFiles;

    public LangManager(UwPlugin plugin, Mode mode, String language, List<String> languages) {
        this.plugin = plugin;

        this.mode = mode;
        this.language = language;
        this.languages = languages;

        this.langFiles = new ArrayList<>();
        languages.forEach(lang -> langFiles.add(new LangFile(plugin, lang)));
    }

    public Mode getMode() {
        return this.mode;
    }

    public String getDefaultLang() {
        return this.language;
    }

    public LangFile getLangFile(String locale) {
        return this.langFiles.stream().filter(langFile -> langFile.getLocale().equalsIgnoreCase(locale)).findFirst().get();
    }

}
