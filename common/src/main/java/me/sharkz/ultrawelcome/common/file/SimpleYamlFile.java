package me.sharkz.ultrawelcome.common.file;

import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import org.simpleyaml.configuration.file.YamlFile;
import org.simpleyaml.exceptions.InvalidConfigurationException;

import java.io.IOException;

public abstract class SimpleYamlFile {

    private final UwPlugin plugin;

    public final YamlFile yamlFile;
    private final String fileName;

    public SimpleYamlFile(UwPlugin plugin, String fileName, boolean loadDefaultFile) {
        this.plugin = plugin;

        // Create new YAML file
        this.fileName = fileName;
        this.yamlFile = new YamlFile(String.format("%s/%s", plugin.getDataFolder(), this.fileName));

        reload(loadDefaultFile);
    }

    public void save() {
        try {
            yamlFile.save();
        } catch (IOException e) {
            plugin.getLogging().severe("Unable to save file: " + fileName);
            e.printStackTrace();
        }
    }

    public void reload(boolean loadDefaultFile) {
        // Load the YAML file if is already created or create new one otherwise
        try {
            if (!yamlFile.exists()) {
                yamlFile.createNewFile(false);
                if (loadDefaultFile) {
                    yamlFile.load(plugin.getResourceAsStream(fileName));
                    yamlFile.save();
                }
            }
            yamlFile.loadWithComments();
        } catch (IOException | InvalidConfigurationException e) {
            plugin.getLogging().severe("Unable to create or load file: " + fileName);
            e.printStackTrace();
        }
    }

}
