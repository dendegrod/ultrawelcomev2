package me.sharkz.ultrawelcome.common.file;

import me.sharkz.ultrawelcome.common.file.language.LangManager;
import me.sharkz.ultrawelcome.common.file.language.Mode;
import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import me.sharkz.ultrawelcome.common.storage.StorageType;
import me.sharkz.ultrawelcome.common.storage.misc.PoolSettings;
import me.sharkz.ultrawelcome.common.storage.misc.StorageCredentials;
import org.simpleyaml.configuration.ConfigurationSection;

import java.util.*;

public abstract class Config extends SimpleYamlFile {

    private final UwPlugin plugin;

    public Config(UwPlugin plugin) {
        super(plugin, "config.yml", true);

        this.plugin = plugin;

        loadData();
    }

    public void reload() {
        reload(false);
        loadData();
    }

    private LangManager langManager;
    public LangManager getLangManager() {
        return langManager;
    }

    private StorageType storageType;
    private StorageCredentials storageCredentials;
    private PoolSettings storagePoolSettings;
    private Map<String, String> storageProperties;
    private String tablePrefix;
    private String mongodbCollectionPrefix;
    private String mongodbConnectionUri;
    public StorageType getStorageType() {
        return storageType;
    }
    public StorageCredentials getStorageCredentials() {
        return storageCredentials;
    }
    public PoolSettings getStoragePoolSettings() {
        return storagePoolSettings;
    }
    public Map<String, String> getStorageProperties() {
        return storageProperties;
    }
    public String getTablePrefix() {
        return tablePrefix;
    }
    public String getMongodbCollectionPrefix() {
        return mongodbCollectionPrefix;
    }
    public String getMongodbConnectionUri() {
        return mongodbConnectionUri;
    }

    private void loadData() {
        // Load LangManger
        ConfigurationSection lang = yamlFile.getConfigurationSection("language");
        String langMode = lang.getString("mode", "global");
        String langLanguage = lang.getString("language", "en_US");
        List<String> langLanguages = lang.getStringList("languages");
        this.langManager = new LangManager(this.plugin, Mode.valueOf(langMode.toUpperCase()), langLanguage, langLanguages);

        // Load storage settings
        ConfigurationSection storage = yamlFile.getConfigurationSection("storage");

        StorageType storageType = StorageType.parse(storage.getString("type"), StorageType.H2);

        ConfigurationSection credentials = storage.getConfigurationSection("credentials");
        StorageCredentials storageCredentials = new StorageCredentials(
                credentials.getString("address", "localhost"),
                credentials.getString("database", "minecraft"),
                credentials.getString("username", "root"),
                credentials.getString("password", "")
        );

        ConfigurationSection poolSettings = storage.getConfigurationSection("pool-settings");
        PoolSettings storagePoolSettings = new PoolSettings(
                poolSettings.getInt("maximum-pool-size", 10),
                poolSettings.getInt("minimum-idle", 10),
                poolSettings.getInt("max-lifetime", 1800000),
                poolSettings.getInt("keepalive-time", 0),
                poolSettings.getInt("connection-timeout", 5000)
        );

        ConfigurationSection properties = storage.getConfigurationSection("properties");
        Map<String, String> storageProperties = new HashMap<>();
        for (String path : properties.getKeys(false)) {
            storageProperties.put(path, properties.getString(path));
        }

        String tablePrefix = storage.getString("table-prefix", "ultrawelcome_");

        String mongodbCollectionPrefix = storage.getString("mongodb-collection-prefix", "");

        String mongodbConnectionUri = storage.getString("mongodb-connection-uri", "");

        this.storageType = storageType;
        this.storageCredentials = storageCredentials;
        this.storagePoolSettings = storagePoolSettings;
        this.storageProperties = storageProperties;
        this.tablePrefix = tablePrefix;
        this.mongodbCollectionPrefix = mongodbCollectionPrefix;
        this.mongodbConnectionUri = mongodbConnectionUri;
    }

}
