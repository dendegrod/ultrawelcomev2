package me.sharkz.ultrawelcome.common.plugin;

import me.sharkz.ultrawelcome.common.file.Config;
import me.sharkz.ultrawelcome.common.file.language.LangManager;
import me.sharkz.ultrawelcome.common.player.AbstractUwPlayer;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.UUID;
import java.util.logging.Logger;

public interface UwPlugin {

    /**
     * Get an instance of this plugin
     *
     * @return Instance of this plugin
     */
    UwPlugin getPlugin();

    /**
     * Get the plugin logger
     *
     * @return Logger
     */
    Logger getLogging();

    /**
     * Get registered players (players present in the data file)
     *
     * @return A collection of players
     */
    Collection<AbstractUwPlayer> getPlayers();

    /**
     * Add a player to the registered players map
     *
     * @param player Player to be added
     */
    void addPlayer(AbstractUwPlayer player);

    /**
     * Get a registered player from the map
     *
     * @param uuid UUID of the player
     * @return Player if present
     */
    AbstractUwPlayer getPlayer(UUID uuid);

    /**
     * Get if a registered player with the specified UUID is present in the map
     *
     * @param uuid UUID of the player
     * @return If the player is present
     */
    boolean isRegistered(UUID uuid);

    /**
     * Get plugin data folder directory
     *
     * @return Data folder directory
     */
    File getDataFolder();

    /**
     * Get a resource by name as an InputStream
     *
     * @param resourceName Name of the resource to get
     * @return The resource as an InputStream
     */
    default InputStream getResourceAsStream(String resourceName) {
        return this.getClass().getResourceAsStream(resourceName);
    }

    /**
     * Get the config
     *
     * @return The config
     */
    Config getConfig();

    /**
     * Get the language manager
     *
     * @return The language manager
     */
    default LangManager getLangManager() {
        return getConfig().getLangManager();
    };

}
