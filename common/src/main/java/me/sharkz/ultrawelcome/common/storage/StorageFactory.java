package me.sharkz.ultrawelcome.common.storage;

import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import me.sharkz.ultrawelcome.common.storage.implementation.StorageImplementation;
import me.sharkz.ultrawelcome.common.storage.implementation.file.YamlStorage;
import me.sharkz.ultrawelcome.common.storage.implementation.mongodb.MongoStorage;
import me.sharkz.ultrawelcome.common.storage.implementation.sql.SqlStorage;
import me.sharkz.ultrawelcome.common.storage.implementation.sql.connection.file.H2ConnectionFactory;
import me.sharkz.ultrawelcome.common.storage.implementation.sql.connection.hikari.MariaDbConnectionFactory;
import me.sharkz.ultrawelcome.common.storage.implementation.sql.connection.hikari.MySqlConnectionFactory;
import me.sharkz.ultrawelcome.common.storage.implementation.sql.connection.hikari.PostgreConnectionFactory;

import java.io.File;

public class StorageFactory {

    private final UwPlugin plugin;

    public StorageFactory(UwPlugin plugin) {
        this.plugin = plugin;
    }

    public Storage getInstance() {
        StorageType type = this.plugin.getConfig().getStorageType();
        this.plugin.getLogging().info(String.format("Loading storage provider... [%s]", type.getName()));
        Storage storage = new Storage(this.plugin, createNewImplementation(type));

        storage.init();
        return storage;
    }

    private StorageImplementation createNewImplementation(StorageType type) {
        switch (type) {
            case YAML:
                return new YamlStorage(this.plugin);
            case MYSQL:
                return new SqlStorage(
                        plugin,
                        new MySqlConnectionFactory(plugin.getConfig().getStorageCredentials()),
                        plugin.getConfig().getTablePrefix()
                );
            case MARIADB:
                return new SqlStorage(
                        plugin,
                        new MariaDbConnectionFactory(plugin.getConfig().getStorageCredentials()),
                        plugin.getConfig().getTablePrefix()
                );
            case POSTGRESQL:
                return new SqlStorage(
                        plugin,
                        new PostgreConnectionFactory(plugin.getConfig().getStorageCredentials()),
                        plugin.getConfig().getTablePrefix()
                );
            case MONGODB:
                return new MongoStorage(plugin, plugin.getConfig().getStorageCredentials(), plugin.getConfig().getMongodbCollectionPrefix(), plugin.getConfig().getMongodbConnectionUri());
            case H2:
                return new SqlStorage(
                        plugin,
                        new H2ConnectionFactory(new File(plugin.getDataFolder(), "data/data-h2")),
                        plugin.getConfig().getTablePrefix()
                );
            default:
                throw new RuntimeException("Unknown storage type: " + type);
        }
    }

}
