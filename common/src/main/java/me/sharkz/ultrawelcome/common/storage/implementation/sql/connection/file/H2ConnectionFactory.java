package me.sharkz.ultrawelcome.common.storage.implementation.sql.connection.file;

import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import me.sharkz.ultrawelcome.common.storage.implementation.sql.connection.ConnectionFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.function.Function;

public class H2ConnectionFactory implements ConnectionFactory {

    private Connection connection;
    private final File file;

    public H2ConnectionFactory(File file) {
        this.file = file;
    }

    @Override
    public String getImplementationName() {
        return "H2";
    }

    @Override
    public void init(UwPlugin plugin) {
    }

    @Override
    public void shutdown() throws Exception {
        if (this.connection != null) {
            this.connection.close();
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (this.connection == null || this.connection.isClosed()) this.connection = DriverManager.getConnection("jdbc:h2:" + file.toString());
        return this.connection;
    }

    @Override
    public Function<String, String> getStatementProcessor() {
        return s -> s.replace("LIKE", "ILIKE").replace("'", "`");
    }
}
