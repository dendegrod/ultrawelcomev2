package me.sharkz.ultrawelcome.common.storage;

import java.util.Arrays;
import java.util.List;

public enum StorageType {

    // File
    YAML("YAML", "yaml", "yml"),

    // Remote database
    MYSQL("MySQL", "mysql"),
    MARIADB("MariaDB", "mariadb"),
    POSTGRESQL("PostgreSQL", "postgresql"),
    MONGODB("MongoDB", "mongodb"),

    // Local database
    H2("H2", "h2"),
    ;

    private final String name;
    private final List<String> identifiers;
    StorageType(String name, String... identifiers) {
        this.name = name;
        this.identifiers = Arrays.asList(identifiers);
    }

    public static StorageType parse(String name, StorageType def) {
        for (StorageType storageType : StorageType.values()) {
            for (String identifier : storageType.getIdentifiers()) {
                if (name.equalsIgnoreCase(identifier)) {
                    return storageType;
                }
            }
        }

        return def;
    }

    public String getName() {
        return name;
    }

    public List<String> getIdentifiers() {
        return identifiers;
    }
}
