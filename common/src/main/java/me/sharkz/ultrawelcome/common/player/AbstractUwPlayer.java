package me.sharkz.ultrawelcome.common.player;

import me.sharkz.ultrawelcome.common.file.language.LangManager;
import me.sharkz.ultrawelcome.common.file.language.Mode;
import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

public abstract class AbstractUwPlayer extends AbstractUwSender {

    public AbstractUwPlayer(UwPlugin plugin) {
        super(plugin);

        LangManager langManager = plugin.getLangManager();
        setLocale(langManager.getMode() == Mode.GLOBAL ? langManager.getDefaultLang() : null); // TODO Implement player data
    }

    /**
     * Get if the player is offline or not
     *
     * @return Whether the player is offline
     */
    public abstract boolean isOffline();

    /**
     * Get player UUID
     *
     * @return Player UUID
     */
    public abstract UUID getUniqueId();

    /**
     * Get player name
     *
     * @return Player name
     */
    public abstract String getName();

    /**
     * Get if the player has the given permission
     *
     * @param permission Permission to check
     * @return Whether the player has the permission
     */
    public abstract boolean hasPermission(@Nullable String permission);

}
