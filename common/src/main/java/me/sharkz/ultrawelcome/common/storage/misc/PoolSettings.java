package me.sharkz.ultrawelcome.common.storage.misc;

public class PoolSettings {

    private final int maximumPoolSize;
    private final int minimumIdle;
    private final int maxLifetime;
    private final int keepaliveTime;
    private final int connectionTimeout;

    public PoolSettings(int maximumPoolSize, int minimumIdle, int maxLifetime, int keepaliveTime, int connectionTimeout) {
        this.maximumPoolSize = maximumPoolSize;
        this.minimumIdle = minimumIdle;
        this.maxLifetime = maxLifetime;
        this.keepaliveTime = keepaliveTime;
        this.connectionTimeout = connectionTimeout;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public int getMinimumIdle() {
        return minimumIdle;
    }

    public int getMaxLifetime() {
        return maxLifetime;
    }

    public int getKeepaliveTime() {
        return keepaliveTime;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }
}
