package me.sharkz.ultrawelcome.common.storage.misc;

public class StorageCredentials {

    private final String address;
    private final String database;
    private final String username;
    private final String password;

    public StorageCredentials(String address, String database, String username, String password) {
        this.address = address;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public String getDatabase() {
        return database;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
