package me.sharkz.ultrawelcome.common.storage;

import me.sharkz.ultrawelcome.common.plugin.UwPlugin;
import me.sharkz.ultrawelcome.common.storage.implementation.StorageImplementation;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

public class Storage {

    private final UwPlugin plugin;
    private final StorageImplementation storageImplementation;

    public Storage(UwPlugin plugin, StorageImplementation storageImplementation) {
        this.plugin = plugin;
        this.storageImplementation = storageImplementation;
    }

    public StorageImplementation getStorageImplementation() {
        return storageImplementation;
    }

    private <T> CompletableFuture<T> future(Callable<T> supplier) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return supplier.call();
            } catch (Exception e) {
                if (e instanceof RuntimeException) {
                    throw (RuntimeException) e;
                }
                throw new CompletionException(e);
            }
        });
    }

    private CompletableFuture<Void> future(Runnable runnable) {
        return CompletableFuture.runAsync(() -> {
            try {
                runnable.run();
            } catch (Exception e) {
                throw (RuntimeException) e;
            }
        });
    }

    public String getName() {
        return this.storageImplementation.getImplementationName();
    }

    public void init() {
        try {
            this.storageImplementation.init();
        } catch (Exception e) {
            this.plugin.getLogging().severe("Failed to init storage implementation");
            e.printStackTrace();
        }
    }

    public void shutdown() {
        try {
            this.storageImplementation.shutdown();
        } catch (Exception e) {
            this.plugin.getLogging().severe("Failed to shutdown storage implementation");
            e.printStackTrace();
        }
    }
}
