package me.sharkz.ultrawelcome.common.player;

import me.sharkz.ultrawelcome.common.file.language.Messages;
import me.sharkz.ultrawelcome.common.plugin.UwPlugin;

public abstract class AbstractUwSender {

    private final UwPlugin plugin;
    private String locale;

    public AbstractUwSender(UwPlugin plugin) {
        this.plugin = plugin;
        this.locale = plugin.getLangManager().getDefaultLang();
    }

    /**
     * Send a given message to the sender
     *
     * @param message Message to send
     */
    public abstract void sendMessage(String message);

    /**
     * Send a given message to the sender
     *
     * @param message
     */
    public void sendMessage(Messages message) {
        sendMessage(plugin.getLangManager().getLangFile(this.locale).getMessage(message.getPath()));
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

}