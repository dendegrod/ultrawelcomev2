package me.sharkz.ultrawelcome.common.storage.implementation;

import me.sharkz.ultrawelcome.common.plugin.UwPlugin;

public interface StorageImplementation {

    UwPlugin getPlugin();

    String getImplementationName();

    void init();

    void shutdown();

}
