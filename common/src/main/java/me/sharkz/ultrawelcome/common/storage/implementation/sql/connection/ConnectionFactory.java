package me.sharkz.ultrawelcome.common.storage.implementation.sql.connection;

import me.sharkz.ultrawelcome.common.plugin.UwPlugin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.Function;

public interface ConnectionFactory {

    String getImplementationName();

    void init(UwPlugin plugin);

    void shutdown() throws Exception;

    Connection getConnection() throws SQLException;

    Function<String, String> getStatementProcessor();

}
